#!/usr/bin/env bash
# fail if any commands fails
set -e
# debug log
set -x
cd $ANDROID_HOME
rm -rf $ANDROID_HOME/emulator
wget -q https://dl.google.com/android/repository/emulator-darwin-5346014.zip
apt-get install zip -y
unzip -q emulator-darwin-5346014.zip
$ANDROID_HOME/tools/bin/sdkmanager "system-images;android-28;google_apis;x86"
$ANDROID_HOME/tools/bin/sdkmanager --licenses
$ANDROID_HOME/tools/bin/avdmanager create avd -n test -k "system-images;android-28;google_apis;x86" -b x86 -c 100M -d 7 -f
ls -al $ANDROID_HOME/emulator
echo no | $ANDROID_HOME/tools/bin/avdmanager create avd -n emulator-5554 -k "system-images;android-28;google_apis;x86" -d pixel --force
cd -

$ANDROID_HOME/emulator/emulator -list-avds
$ANDROID_HOME/tools/bin/avdmanager list avd