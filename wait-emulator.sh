#!/usr/bin/env bash
# fail if any commands fails
set -e

$ANDROID_HOME/emulator/emulator-headless -no-window -no-boot-anim -gpu swiftshader_indirect -no-audio -camera-back none -camera-front none -no-snapshot @$($ANDROID_HOME/emulator/emulator -list-avds | head -n 1) > /dev/null 2>&1 &

echo "Waiting for Emulator"
echo $($ANDROID_HOME/emulator/emulator -list-avds | head -n 1)

bootanim=""
failcounter=0
timeout_in_sec=600

until [[ "$bootanim" =~ "stopped" ]]; do
  bootanim=`adb -e shell getprop init.svc.bootanim 2>&1 &`
  if [[ "$bootanim" =~ "device not found" || "$bootanim" =~ "device offline" || "$bootanim" =~ "no emulators found"
    || "$bootanim" =~ "running" ]]; then
    let "failcounter += 1"
    echo "."
    if [[ $failcounter -gt timeout_in_sec ]]; then
      echo "Timeout ($timeout_in_sec seconds) reached; failed to start emulator"
      exit 1
    fi
  fi
  sleep 1
done

echo "Emulator is ready"
sleep 60